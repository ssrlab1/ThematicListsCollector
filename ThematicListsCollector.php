<?php
    class ThematicListsCollector
	{
		private static $localizationArr = array();
		private static $localizationErr = array();
		private $result = array();
		private $statistics = "";
		const BR = "<br>\n";
		
		function __construct() {

		}
		
		public static function loadLanguages() {
			$languages = array();
			$files = scandir('lang/');
			if(!empty($files)) {
				foreach($files as $file) {
					if(substr($file, 2, 4) == '.txt') {
						$languages[] = substr($file, 0, 2);
					}
				}
			}
			return $languages;
		}
		
		public static function loadLocalization($lang) {
			$filepath = "lang/$lang.txt";
			$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			if($linesArr === false) {
				$filepath = "lang/en.txt";
				$linesArr = file($filepath, FILE_IGNORE_NEW_LINES);
			}
			foreach($linesArr as $line) {
				if(empty($line)) {
					$key = $value = '';
				}
				elseif(substr($line, 0, 1) !== '#' && substr($line, 0, 2) !== '//') {
					if(empty($key)) {
						$key = $line;
					}
					else {
						if(!isset(self::$localizationArr[$key])) {
							self::$localizationArr[$key] = $line;
						}
					}
				}
			}
		}
		
		public static function showMessage($msg) {
			if(isset(self::$localizationArr[$msg])) {
				return self::$localizationArr[$msg];
			}
			else {
				self::$localizationErr[] = $msg;
				return $msg;
			}
		}
		
		public static function sendErrorList() {
			if(!empty(self::$localizationErr)) {
				$ip = str_replace('.', '-', $_SERVER['REMOTE_ADDR']);
				$sendersName = 'Thematic Lists Collector';
				$recipient = 'corpus.by@gmail.com';
				$subject = "ERROR: Incorrect localization in $sendersName by user $ip";
				$mailBody = 'Вітаю супрацоўнікаў Лабараторыі распазнавання і сінтэзу маўлення!' . self::BR;
				$mailBody .= $_SERVER['HTTP_HOST'] . '/ThematicListsCollector/ дасылае інфармацыю аб наступных памылках:' . self::BR . self::BR;
				$mailBody .= 'Адсутнічае лакалізацыя наступных ідэнтыфікатараў:' . self::BR . implode(self::BR, self::$localizationErr) . self::BR;
				$header  = "MIME-Version: 1.0\r\n";
				$header .= "Content-type: text/html; charset=utf-8\r\n";
				$header .= "From: $sendersName <corpus.by@gmail.com>\r\n";
				mail($recipient, $subject, $mailBody, $header);
			}
		}
		
		public static function sortBel($a, $b) {
			$a = str_replace(array("'", "-", "+", "="), "", mb_strtoupper($a, 'UTF-8'));
			$b = str_replace(array("'", "-", "+", "="), "", mb_strtoupper($b, 'UTF-8'));
			
			$alphabet = array(
				'А' => 1, 'Б' => 2, 'В' => 3, 'Г' => 4, 'Д' => 5, 'Е' => 6, 'Ё' => 7, 'Ж' => 8, 'З' => 9, 'І' => 10, 'Й' => 11,
				'К' => 12, 'Л' => 13, 'М' => 14, 'Н' => 15, 'О' => 16, 'П' => 17, 'Р' => 18, 'С' => 19, 'Т' => 20, 'У' => 21, 'Ў' => 22,
				'Ф' => 23, 'Х' => 24, 'Ц' => 25, 'Ч' => 26, 'Ш' => 27, 'Ы' => 28, 'Ь' => 29, 'Э' => 30, 'Ю' => 31, 'Я' => 32
			);
			
			$lengthA = mb_strlen($a, 'UTF-8');
			$lengthB = mb_strlen($b, 'UTF-8');
			$status = 0;
			
			for($i = 0; $i < ($lengthA > $lengthB ? $lengthB : $lengthA); $i++) {
				$substr_a = mb_substr($a, $i, 1, 'UTF-8');
				$substr_b = mb_substr($b, $i, 1, 'UTF-8');
				if(isset($alphabet[$substr_a]) && isset($alphabet[$substr_b]) && $alphabet[$substr_a] < $alphabet[$substr_b]) {
					$status = -1;
					break;
				}
				elseif(isset($alphabet[$substr_a]) && isset($alphabet[$substr_b]) && $alphabet[$substr_a] > $alphabet[$substr_b]) {
					$status = 1;
					break;
				}
				else {
					$status = 0;
				}
			}
			if($status != 0) {
				return $status;
			}
			else {
				return ($lengthA < $lengthB ? -1 : 1);
			}
		}
	
		public function generateInfoOnAllophones($mode = 1) {
			$topicsCnt = 0;
			$wordsCnt = 0;
			include 'ExcelReader/reader.php';
			$excel = new Spreadsheet_Excel_Reader();
			$excel->setOutputEncoding('UTF-8');
			$excel->read('in/Allophones.xls');
			if($mode == 1) $sheet = 0;
			if($mode == 2) $sheet = 1;
			if(isset($excel->sheets[$sheet]["cells"])) {
				foreach($excel->sheets[$sheet]["cells"] as $row => $colsArr) {
					if(isset($colsArr[3]) && isset($colsArr[4]) && $colsArr[4] !== 'Тэматыка') {
						$word = mb_strtolower(trim($colsArr[3]), 'UTF-8');
						$domen = mb_strtolower(trim($colsArr[4]), 'UTF-8');
						if(!isset($this->result[$domen])) {
							$this->result[$domen][] = $word;
							$wordsCnt++;
							$topicsCnt++;
						}
						else {
							if(!in_array($word, $this->result[$domen])) {
								$this->result[$domen][] = $word;
								$wordsCnt++;
							}
						}
					}
				}
				uksort($this->result, array('ThematicListsCollector', 'sortBel'));
				$average = round($wordsCnt / $topicsCnt, 1);
				
				$this->statistics = "Topics = $topicsCnt, Words = $wordsCnt, Words per Topic = $average";
				foreach($this->result as $topic => $words) {
					usort($words, array('ThematicListsCollector', 'sortBel'));
					$this->result[$topic] = $words;
				}
			}
			else {
				echo 'ERROR! Nothing was read from file!<br>';
				// print_r($excel->sheets);
			}
		}
		
		public function readInFile_2Robots($filename = '') {
			$filePath = dirname(__FILE__) . "/in/2Robots_v2_Main.dic";
			if(empty($filename)) $filePath .= "";
			else $filePath .= $filename;
			
			if(!file_exists($filePath)) mkdir($filePath);
			
			$fp = fopen($filePath, 'r') OR die('fail open file');
			if($fp){
				$this->statistics = "<br/> The in file is <b>" . filesize($filePath)/1024/1024 . " Megabytes</b><br/><br/>";
				$cnt = 0;
				while(($line = fgets($fp, 4096)) !== false){
					if(substr($line, 0, 1) != "#" && !empty($line)){
						$cnt++;
						$line = preg_replace("/,GUID.*/u", "", $line);
						$this->result["Robots"][] = trim($line);
					}
				}
				//$this->result .= "<br><h4>Lines = $cnt</h4>";
			}
			fclose($fp);
		}
		
		public function readInFile_3Robots($filename = '') {
			$filePath = dirname(__FILE__) . "/in/3Robots_v1_Main.dic";
			if (empty($filename)) {
				$filePath .= "";
			}
			else {
				$filePath .= $filename;
			}
			if(!file_exists($filePath)) mkdir($filePath);
			$fp = fopen($filePath, 'r') OR die('fail open file');
			if($fp) {
				$this->statistics = "<br/> The in file is <b>" . filesize($filePath)/1024/1024 . " Megabytes</b><br/><br/>";
				$cnt = 0;
				while(($line = fgets($fp, 4096)) !== false) {
					if(substr($line, 0, 1) != "#" && !empty($line)) {
						$cnt++;
						$line = preg_replace("/,GUID.*/u", "", $line);
						$this->result["Robots"][] = trim($line);
					}
				}
				//$this->result .= "<br><h4>Lines = $cnt</h4>";
			}
			fclose($fp);
		}
		
		public function getResult() {
			return $this->result;
		}
		
		public function getStatistics() {
			return $this->statistics;
		}
	}
?>