<?php
	header("Content-type: text/html;  charset=utf-8");
	header("Access-Control-Allow-Origin: *");
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8');
		
	$localization = isset($_POST['localization']) ? $_POST['localization'] : 'en';
	$selector = isset($_POST['selector']) ? $_POST['selector'] : 'readWordsFromTableArrAllophones';
	
	include_once 'ThematicListsCollector.php';
	ThematicListsCollector::loadLocalization($localization);
	
	$msg = '';
	if(!empty($selector)) {
		$result = array();
		$selector = trim($selector);
		$ThematicListsCollector = new ThematicListsCollector();
		if(strcmp($selector, 'readWordsFromTableArrAllophones') == 0) {
			$ThematicListsCollector->generateInfoOnAllophones(1);
		}
		if(!empty($_POST) && strcmp($_POST['selector'], 'readWordsFromTableArrDiphones') == 0) {
			$ThematicListsCollector->generateInfoOnAllophones(2);
		}
		if(!empty($_POST) && strcmp($_POST['selector'], 'readInFile_2Robots') == 0) {
			$ThematicListsCollector->readInFile_2Robots();
		}
		if(!empty($_POST) && strcmp($_POST['selector'], 'readInFile_3Robots') == 0) {
			$ThematicListsCollector->readInFile_3Robots();
		}
		$result['result'] = $ThematicListsCollector->getResult();
		$result['statistics'] = $ThematicListsCollector->getStatistics();
		$msg = json_encode($result);
	}
	echo $msg;
?>