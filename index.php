<?php
	header("Content-Type: text/html; charset=utf-8");
	$ini = parse_ini_file('service.ini');
	include_once 'ThematicListsCollector.php';
	$lang = isset($_GET['lang']) ? $_GET['lang'] : 'en';
	$languages = ThematicListsCollector::loadLanguages();
	ThematicListsCollector::loadLocalization($lang);
?>
<!DOCTYPE html>
<html lang="<?php echo $lang; ?>">
	<head>
		<title><?php echo ThematicListsCollector::showMessage('title'); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href='css/theme.css'>
		<link rel="icon" type="image/x-icon" href="img/favicon.ico">
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
		<?php include_once 'analyticstracking.php'; ?>
		<script>
			$(document).ready(function () {
				$('button#MainButtonId').click(function() {
					$.ajax({
						type: 'POST',
						url: 'https://corpus.by/ThematicListsCollector/api.php',
						data: {
							'localization': '<?php echo $lang; ?>',
							'selector': $('select#selectorId').val()
						},
						success: function(msg) {
							$('#resultId').empty();
							output = document.getElementById("resultId");
							var result = jQuery.parseJSON(msg);
							
							var title = "";
							if($('select#selectorId').val() == "readWordsFromTableArrAllophones") {
								title = "Read Data from Xslx File (Allophones)";
							}
							else if($('select#selectorId').val() == "readWordsFromTableArrDiphones") {
								title = "Read Data from Xslx File (Diphones)";
							}
							
							let titleElem = document.createElement("h3");
							titleElem.innerHTML = title;
							output.appendChild(titleElem);
							
							let statisticsElem = document.createElement("h4");
							statisticsElem.innerHTML = result.statistics;
							output.appendChild(statisticsElem);

							let entry = document.createElement("div");
							entry.innerHTML = Object.keys(result.result).reduce(
								function(previousValue, currentValue) {
									return(previousValue
										+ "<br/><h4>Тэматы+чны даме+н: "
										+ currentValue + "..##</h4><br/>"
										+ result.result[currentValue].join(".#<br/>") + "<br/>"
									);
								},
								"",
							)
							output.appendChild(entry);
							$('#resultBlockId').show('slow');
						},
						error: function() {
							$('#resultId').html('ERROR');
						}
					});
				});
			});
		</script>
	</head>
	<body>
		<!-- Novigation -->
		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/"><img style="max-width:150px; margin-top: -7px;" src="img/main-logo.png" alt=""></a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="service-name"><a href="/ThematicListsCollector/?lang=<?php echo $lang; ?>"><?php echo ThematicListsCollector::showMessage('title'); ?></a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li class="service-name"><a href="<?php echo ThematicListsCollector::showMessage('help'); ?>" target="_blank">?</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo ThematicListsCollector::showMessage($lang); ?><span class="caret"></span></a>
							<ul class="dropdown-menu">
								<?php
									$languages = ThematicListsCollector::loadLanguages();
									foreach($languages as $language) {
										echo "<li><a href='?lang=$language'>" . ThematicListsCollector::showMessage($language) . "</a></li>";
									}
								?>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</nav>
		<!-- End of Novigation -->
		<div class="container theme-showcase" role="main">
			<div class="row">
				<div class="col-md-12">
					<div class="control-panel">
						<select name='selector' id="selectorId" class="selector-primary">
							<option value='readWordsFromTableArrAllophones'><?php echo ThematicListsCollector::showMessage('button1'); ?></option>
							<option value='readWordsFromTableArrDiphones'><?php echo ThematicListsCollector::showMessage('button2'); ?></option>
							<option value='readInFile_2Robots'><?php echo ThematicListsCollector::showMessage('button3'); ?></option>
							<option value='readInFile_3Robots'><?php echo ThematicListsCollector::showMessage('button4'); ?></option>
						</select>
						<button type="button" id="MainButtonId" name="MainButton" class="button-primary"><?php echo ThematicListsCollector::showMessage('button'); ?></button>
					</div>
				</div>
				<div class="col-md-12" id="resultBlockId" style="display: none;">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3 class="panel-title"><?php echo ThematicListsCollector::showMessage('result'); ?></h3>
							</div>
							<div class="panel-body">
								<p id="resultId"></p>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="control-panel">
						<div class="panel panel-default">
							<div class="panel-body">
								<?php echo ThematicListsCollector::showMessage('service code'); ?>&nbsp;<a href="https://gitlab.com/ssrlab1/ThematicListsCollector" target="_blank"><?php echo ThematicListsCollector::showMessage('reference'); ?></a>.
								<br />
								<a href="https://gitlab.com/ssrlab1/ThematicListsCollector/-/issues/new" target="_blank"><?php echo ThematicListsCollector::showMessage('suggestions'); ?></a>.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<footer class="footer">
			<div class="container">
				<p class="text-muted">
					<?php echo ThematicListsCollector::showMessage('contact e-mail'); ?>
					<a href="mailto:corpus.by@gmail.com">corpus.by@gmail.com</a>.<br />
					<?php echo ThematicListsCollector::showMessage('other prototypes'); ?>
					<a href="https://corpus.by/?lang=<?php echo $lang; ?>">corpus.by</a>,&nbsp;<a href="https://ssrlab.by">ssrlab.by</a>.
				</p>
				<p class="text-muted">
					<?php echo ThematicListsCollector::showMessage('laboratory'), ', ', $ini['year']; if($ini['year'] !== date('Y')) echo '—', date('Y'); ?>
				</p>
			</div>
		</footer>
	</body>
</html>
<?php ThematicListsCollector::sendErrorList($lang); ?>